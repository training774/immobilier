package training.Immobilier.experience.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import training.Immobilier.experience.enums.CountriesEnums;
import training.Immobilier.experience.exceptions.ManagementError;
import training.Immobilier.experience.model.request.LoanOffersRequest;
import training.Immobilier.experience.model.request.LoanRequest;
import training.Immobilier.experience.model.responses.ResponseObject;
import training.Immobilier.experience.service.LoanService;

import javax.validation.Valid;
import java.util.Optional;
@Slf4j
@RestController
@RequestMapping("/loan")
@CrossOrigin("*")
public class LoanController {
    @Autowired
    LoanService service;



    @GetMapping("/tenures")
    public ResponseEntity<ResponseObject> getTenures (@RequestHeader("country") CountriesEnums countries, @RequestHeader("uuid") String uuid){
        try {
            log.info("get tenure controller {}",uuid);
            return new ResponseEntity<>(service.getTenure(countries,uuid), HttpStatus.OK);
        }catch (ManagementError e){
            log.error("get tenure controller {} : with error {}",uuid,e.getMessage());
            return new ResponseEntity<>(new ResponseObject(Optional.ofNullable(null), Optional.ofNullable(e.getMessage()),"400"),HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/loan-offers")
    public ResponseEntity<ResponseObject> postLoanOffers (@RequestHeader("country") CountriesEnums countries, @RequestHeader("uuid") String uuid, @RequestBody @Valid LoanOffersRequest loanOffersRequest){
        try {
            log.info("loan offer tenure controller {}",uuid);
            return new ResponseEntity<>(service.getLoanOffers(countries,uuid,loanOffersRequest), HttpStatus.OK);
        }catch (ManagementError e){
            log.error("loan offer controller {} : with {}",uuid,e.getMessage());
            return new ResponseEntity<>(new ResponseObject(Optional.ofNullable(null), Optional.ofNullable(e.getMessage()),"400"),HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("/submission")
    public ResponseEntity<ResponseObject> ConfirmLoanApplication (@RequestHeader("country") CountriesEnums countries, @RequestHeader("uuid") String uuid, @RequestBody @Valid LoanRequest loanRequest){
        try {
            log.info("request received for {}",uuid);
            return new ResponseEntity<>(service.confirmNewLoan(loanRequest,countries,uuid), HttpStatus.OK);
        }catch (ManagementError e){
            log.error("loan offer controller {} : with {}",uuid,e.getMessage());
            return new ResponseEntity<>(new ResponseObject(Optional.ofNullable(null), Optional.ofNullable(e.getMessage()),"400"),HttpStatus.BAD_REQUEST);
        }
    }

}
