package training.Immobilier.experience.service;

import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import training.Immobilier.experience.enums.CountriesEnums;
import training.Immobilier.experience.exceptions.ManagementError;
import training.Immobilier.experience.model.request.LoanOffersRequest;
import training.Immobilier.experience.model.request.LoanRequest;
import training.Immobilier.experience.model.responses.ResponseObject;
import training.Immobilier.experience.repo.ExperienceRepo;

@Slf4j
@Service
public class LoanService {
@Autowired
ExperienceRepo experienceRepo;
    public ResponseObject getTenure(CountriesEnums countriesEnums, String uuid){
        try {
            log.info("get tenure service {}",uuid);
            ResponseObject response = experienceRepo.getTenures(countriesEnums,uuid);
            if (response.getStatus().equals("400") || response.getStatus().equals("500")) {
                log.error("get tenure service {}",uuid);
                throw new ManagementError(response.getError().toString());
            }
            return response;
        }catch (FeignException e){
            log.error("get tenure service {}",uuid);
            throw new ManagementError(e.getMessage());
        }
    }

    public  ResponseObject getLoanOffers( CountriesEnums countriesEnums, String uuid, LoanOffersRequest loanOffersRequest){
        try {
            log.info("request received at service get Loanoffer for {}",uuid);
            ResponseObject response = experienceRepo.getLoanOffers(countriesEnums,uuid,loanOffersRequest);
            log.info("responce recived for {} for management ms",uuid);
            if (response.getStatus().equals("400") || response.getStatus().equals("500")) {
                log.error("{} error in data with error{}",uuid,response.getError().toString());
                throw new ManagementError(response.getError().toString());
            }
            return response;
        }catch (FeignException e){
            log.error("{} error in data ",uuid);
            throw new ManagementError(e.getMessage());
        }
    }




    public ResponseObject confirmNewLoan(LoanRequest loanRequest, CountriesEnums countriesEnums, String uuid) {

        try {
            log.error("Confirm loan service {}",uuid);

            ResponseObject response = experienceRepo.submitLoans(countriesEnums, uuid, loanRequest);
            if (response.getStatus().equals("400") || response.getStatus().equals("500")) {
                log.error("Confirm loan service {} : with error {}",uuid,response.getError().toString());
                throw new ManagementError(response.getError().toString());
            }
            return response;
        } catch (FeignException e) {
            log.error("Confirm loan service {} : with error {}",uuid,e.getMessage());
            throw new ManagementError(e.getMessage());
        }
    }

}
