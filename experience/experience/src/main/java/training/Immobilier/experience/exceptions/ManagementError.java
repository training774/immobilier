package training.Immobilier.experience.exceptions;

public class ManagementError extends RuntimeException{
    public ManagementError(String msg){
        super(msg);
    }
}
