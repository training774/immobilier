package training.Immobilier.experience.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import training.Immobilier.experience.enums.TenureEnum;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoanOffersRequest {
    @Min(value = 600,message = "Loan amout must be in the limit of 600 JOD to 2000 JOD")
    @Max(value = 2000,message = "Loan amout must be in the limit of 600 JOD to 2000 JOD")
    private float amount;
    private TenureEnum tenure;
}

