package training.Immobilier.experience.repo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import training.Immobilier.experience.enums.CountriesEnums;
import training.Immobilier.experience.model.request.LoanOffersRequest;
import training.Immobilier.experience.model.request.LoanRequest;
import training.Immobilier.experience.model.responses.ResponseObject;

@FeignClient(name = "Loan-updated", url = "${config.rest.service.getLoanUrlUpdated}")
public interface ExperienceRepo {

    @GetMapping("/tenures")
    public ResponseObject getTenures(@RequestHeader("country") CountriesEnums countries, @RequestHeader("uuid") String uuid);

    @PostMapping("/loan-offers")
    public ResponseObject getLoanOffers(@RequestHeader("country") CountriesEnums countries, @RequestHeader("uuid") String uuid, @RequestBody LoanOffersRequest loanOffersRequest);

    @PostMapping("submission")
    public ResponseObject submitLoans(@RequestHeader("country") CountriesEnums countries, @RequestHeader("uuid") String uuid, @RequestBody LoanRequest loanRequest);


}
