package training.Immobilier.management.model.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import training.Immobilier.management.model.entity.LoanConfirmed;

public interface    NewLoanRepo extends JpaRepository<LoanConfirmed,Long> {

}
