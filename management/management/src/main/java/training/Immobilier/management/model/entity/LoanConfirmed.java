package training.Immobilier.management.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import training.Immobilier.management.enums.CountriesEnums;
import training.Immobilier.management.enums.LoanStatusEnums;
import training.Immobilier.management.enums.SalaryRangeEnum;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Loan_Request")
public class LoanConfirmed {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Loan_Request_id")
    private Long id;
    @Column(name = "Loan_Request_name")
    private String loanRequestName;
    @Column(name = "Loan_Request_age")
    private int loanRequestAge;
    @Column(name = "Loan_Request_nationalId")
    private String loanRequestNationalId;
    @Column(name = "Loan_Request_loanAmount")
    private float loanRequestLoanAmount;
    @Column(name = "Loan_Request_tenure")
    private int loanRequestTenure;
    @Column(name = "Loan_Request_installment")
    private float loanResponseInstallment;
    @Column(name = "Loan_Request_intrestRate")
    private float loanResponseIntrestRate;
    @Column(name = "Loan_Request_totalIntrestAmount")
    private float loanResponseTotalIntrestAmount;
    @Column(name = "Loan_Request_fees")
    private int loanResponseFees;
    @Column(name = "Loan_Request_total")
    private float loanResponseTotal;
    @Column(name = "Loan_Request_status")
    @Enumerated(EnumType.STRING)
    private LoanStatusEnums loanResponseStatus;
    @Column(name = "Loan_Request_salaryrange")
    @Enumerated(EnumType.STRING)
    private SalaryRangeEnum salaryRangeEnum;

    @CreatedDate
    @Column(name = "Loan_Request_created_date")
   private LocalDateTime createdDate;

    @CreatedBy
    private String createBy;

    @Column(name = "Loan_Request_uuid")
    private String uuid;

    @Column(name = "country")
    @Enumerated(EnumType.STRING)
    private CountriesEnums countriesEnums;


}
