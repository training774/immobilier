package training.Immobilier.management.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import training.Immobilier.management.enums.CountriesEnums;
import training.Immobilier.management.exceptions.TenureException;
import training.Immobilier.management.model.request.LoanOffersRequest;
import training.Immobilier.management.model.request.LoanRequest;
import training.Immobilier.management.model.response.ResponseObject;
import training.Immobilier.management.service.NewLoanService;

import java.util.Optional;
@Slf4j
@RestController

public class NewLoanController {

    @Autowired
    NewLoanService service;
    @GetMapping("/tenures")
    public ResponseEntity<ResponseObject> getTenures(@RequestHeader("country")CountriesEnums countries,@RequestHeader("uuid") String uuid){
        try {
            log.info("get tenures called at {}",uuid);
            return new ResponseEntity<>(new ResponseObject(Optional.ofNullable(service.retriveTenures(uuid)), null, "200"), HttpStatus.OK);
        }catch (Exception e){
            log.error("get tenures error {}",uuid);
            return new ResponseEntity<>(new ResponseObject(null,Optional.ofNullable(e.getMessage()),"400"), HttpStatus.BAD_REQUEST);
        }
        }

    @PostMapping("/loan-offers")
    public ResponseEntity<ResponseObject> postLoanOffers(@RequestHeader("country") CountriesEnums countries,@RequestHeader("uuid") String uuid, @RequestBody LoanOffersRequest loanOffersRequest) {
       try {
           log.info("get loan offers called at {}",uuid);
           return new ResponseEntity<>(new ResponseObject(Optional.ofNullable(service.loanCalculations(loanOffersRequest.getAmount(), loanOffersRequest.getTenure())), null, "200"), HttpStatus.OK);
       }
       catch (TenureException e){
           log.error("error loan offers called by {} : {} ",uuid,e.getMessage());
           return new ResponseEntity<>(new ResponseObject(null,Optional.ofNullable(e.getMessage()),"400"), HttpStatus.BAD_REQUEST);
       }
       catch (Exception e){
           log.error("error loan offers called by {} : {} ",uuid,e.getMessage());
           return new ResponseEntity<>(new ResponseObject(null,Optional.ofNullable(e.getMessage()),"400"), HttpStatus.BAD_REQUEST);
       }
       }

    @PostMapping("/submission")
    public ResponseEntity<ResponseObject> submitLoanApplication(@RequestHeader("country") CountriesEnums countries,@RequestHeader("uuid") String uuid, @RequestBody LoanRequest loanRequest){
       try {

           log.info("post  loan confirmation called by {}  ",uuid);
           return new ResponseEntity<>(new ResponseObject(Optional.ofNullable(service.LoanConfirmed(loanRequest, uuid, countries)), null, "200"), HttpStatus.ACCEPTED);
       }catch (TenureException e){
           log.error("error loan confirmation called by {} : {} ",uuid,e.getMessage());
           return new ResponseEntity<>(new ResponseObject(null,Optional.ofNullable(e.getMessage()),"400"), HttpStatus.OK);
       }
       catch (Exception e){
           log.error("error loan confirmation called by {} : {} ",uuid,e.getMessage());
           return new ResponseEntity<>(new ResponseObject(null,Optional.ofNullable(e.getMessage()),"400"), HttpStatus.OK);
       }
       }


}
