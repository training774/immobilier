package training.Immobilier.management.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import training.Immobilier.management.enums.LoanStatusEnums;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanConfirmationResponse {
private String msg;
private LoanStatusEnums status;
private Long id;
}
