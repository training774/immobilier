package training.Immobilier.management.service;


import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import training.Immobilier.management.enums.CountriesEnums;
import training.Immobilier.management.enums.LoanStatusEnums;
import training.Immobilier.management.enums.SalaryRangeEnum;
import training.Immobilier.management.exceptions.TenureException;
import training.Immobilier.management.model.entity.LoanConfirmed;
import training.Immobilier.management.model.repo.NewLoanRepo;
import training.Immobilier.management.model.request.LoanRequest;
import training.Immobilier.management.model.response.LoanConfirmationResponse;
import training.Immobilier.management.model.response.LoanResponseUpdated;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
@Slf4j
@Service
public class NewLoanService {
    @Autowired
    CalculationService calculationService;

    @Autowired
    NewLoanRepo newLoanRepo;
    @Autowired
    ModelMapper modelMapper;

    public List<String> retriveTenures(String uuid){
        List<String> tenures = new ArrayList<String>();
        tenures.add("12 Months");
        tenures.add("24 Months");
        tenures.add("36 Months");
        tenures.add("48 Months");
        tenures.add("60 Months");
        return tenures;
    }

    public LoanResponseUpdated loanCalculations(float amount,float tenure){
        float[] values = calculationService.calculteFeeAndIntrestrate((int) tenure);
        float intrestRate = values[0];
        float fee = values[1];
        int installment = calculationService.calculateInstallment(amount, intrestRate, tenure);
        int totalAmount = (int) (installment * tenure);
        int totalInterest =  Math.round(totalAmount - amount);
        return new LoanResponseUpdated(installment,intrestRate,totalInterest,fee,totalAmount);
    }


    public LoanConfirmationResponse LoanConfirmed(LoanRequest loanRequest, String uuid, CountriesEnums countriesEnums){

        try {


        float[] values = calculationService.calculteFeeAndIntrestrate((int) loanRequest.getTenure());
        float intrestRate = values[0];
        float fee = values[1];
        int installment = calculationService.calculateInstallment(loanRequest.getLoanAmount(), intrestRate, loanRequest.getTenure());
        int totalAmount = (int) (installment * loanRequest.getTenure());
        int totalInterest =  Math.round(totalAmount - loanRequest.getLoanAmount());

        LoanConfirmed data = modelMapper.map(loanRequest, LoanConfirmed.class);
        data.setId(null);
        data.setLoanResponseInstallment(installment);
        data.setLoanResponseFees((int) fee);
        data.setLoanResponseIntrestRate(intrestRate);
        data.setLoanResponseTotal(totalAmount);
        data.setLoanResponseTotalIntrestAmount(totalInterest);

        data.setCreateBy(loanRequest.getName());
        data.setUuid(uuid);
        data.setCountriesEnums(CountriesEnums.valueOf(countriesEnums.name()));
        data.setLoanResponseStatus(LoanStatusEnums.CONFIRMED);

            data.setCreatedDate(LocalDateTime.now());

        log.info("entity created {}",data.getUuid());
        LoanConfirmed responce = newLoanRepo.save(data);
        log.info("entity inserted into table {}",data.getUuid());

            LoanConfirmationResponse loanConfirmationResponse = new LoanConfirmationResponse("Loan Request Confirmed",responce.getLoanResponseStatus(),responce.getId());
            log.info("sending response {}",data.getUuid());
        return loanConfirmationResponse;}
        catch (TenureException e){
            throw new TenureException();
        }
    }
}
